EESchema Schematic File Version 2
LIBS:monitor-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:btm-222
LIBS:derfmega256-23m00
LIBS:max887
LIBS:mos_p
LIBS:sdcard
LIBS:sd-card-con
LIBS:ufl
LIBS:stratification-lc
LIBS:str-lc-cache
LIBS:at45db161e-shd-b
LIBS:hc-05
LIBS:mcp73833
LIBS:monitor-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date "31 mar 2015"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L GND #PWR01
U 1 1 533AAFF5
P 1200 6850
F 0 "#PWR01" H 1200 6850 30  0001 C CNN
F 1 "GND" H 1200 6780 30  0001 C CNN
F 2 "" H 1200 6850 60  0001 C CNN
F 3 "" H 1200 6850 60  0001 C CNN
	1    1200 6850
	1    0    0    -1  
$EndComp
Text Label 2650 5600 2    60   ~ 0
VCC
Text Label 2650 5800 2    60   ~ 0
GND
Text Label 2650 5400 2    60   ~ 0
SPI_nSDCS
Text Label 2650 5900 2    60   ~ 0
SPI_MISO
Text Label 2650 5700 2    60   ~ 0
SPI_SCK
Text Label 2650 5500 2    60   ~ 0
SPI_MOSI
$Comp
L SD-CARD-CON C12
U 1 1 533AAD38
P 2050 5300
F 0 "C12" H 2450 4300 60  0000 C CNN
F 1 "MCC-SDMICRO/2" H 2700 5400 60  0000 C CNN
F 2 "uSDCARD:uSDCARD" H 2050 5300 60  0001 C CNN
F 3 "" H 2050 5300 60  0001 C CNN
	1    2050 5300
	-1   0    0    -1  
$EndComp
Text Label 6350 6950 2    60   ~ 0
VCC
Text Label 6350 7250 2    60   ~ 0
GND
Text Label 6350 6850 2    60   ~ 0
GND
Text Label 4850 6850 0    60   ~ 0
JTAG_TCK
Text Label 4850 6950 0    60   ~ 0
JTAG_TDO
Text Label 4850 7250 0    60   ~ 0
JTAG_TDI
Text Label 4850 7050 0    60   ~ 0
JTAG_TMS
$Comp
L AVR-JTAG-10 CON1
U 1 1 51C49800
P 5750 7050
F 0 "CON1" H 5580 7380 50  0000 C CNN
F 1 "AVR-JTAG-10" H 5410 6720 50  0000 L BNN
F 2 "ZL322-2X5P:ZL322-2X5P" V 5180 7070 50  0001 C CNN
F 3 "" H 5750 7050 60  0001 C CNN
	1    5750 7050
	1    0    0    -1  
$EndComp
$Comp
L CP1-RESCUE-monitor C7
U 1 1 50713F0E
P 8550 2350
F 0 "C7" H 8600 2450 50  0000 L CNN
F 1 "10uF/6V" V 8800 2200 50  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:TantalC_SizeA_EIA-3216_HandSoldering" H 8550 2350 60  0001 C CNN
F 3 "" H 8550 2350 60  0001 C CNN
	1    8550 2350
	1    0    0    -1  
$EndComp
Text Label 3600 2350 3    60   ~ 0
JTAG_TCK
Text Label 3800 2350 3    60   ~ 0
JTAG_TDO
Text Label 3700 2350 3    60   ~ 0
JTAG_TMS
Text Label 3900 2350 3    60   ~ 0
JTAG_TDI
$Comp
L R-RESCUE-monitor R5
U 1 1 530BAEA6
P 9800 1100
F 0 "R5" V 9880 1100 50  0000 C CNN
F 1 "10k" V 9800 1100 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 9800 1100 60  0001 C CNN
F 3 "" H 9800 1100 60  0001 C CNN
	1    9800 1100
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 530BAEAC
P 9800 2100
F 0 "#PWR02" H 9800 2100 30  0001 C CNN
F 1 "GND" H 9800 2030 30  0001 C CNN
F 2 "" H 9800 2100 60  0001 C CNN
F 3 "" H 9800 2100 60  0001 C CNN
	1    9800 2100
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR03
U 1 1 530BAEB2
P 9550 700
F 0 "#PWR03" H 9550 800 30  0001 C CNN
F 1 "VCC" H 9550 800 30  0000 C CNN
F 2 "" H 9550 700 60  0001 C CNN
F 3 "" H 9550 700 60  0001 C CNN
	1    9550 700 
	1    0    0    -1  
$EndComp
$Comp
L MCP121 U5
U 1 1 530BAEB8
P 9550 1450
F 0 "U5" V 10100 1350 60  0000 C CNN
F 1 "MCP121T-270E/TT" V 10000 1450 60  0000 C CNN
F 2 "Housings_SOT-23_SOT-143_TSOT-6:SOT-23_Handsoldering" H 9550 1450 60  0001 C CNN
F 3 "" H 9550 1450 60  0001 C CNN
	1    9550 1450
	-1   0    0    -1  
$EndComp
Text Label 3500 5650 1    60   ~ 0
~RST
Text Label 6350 7050 2    60   ~ 0
~RST
$Comp
L SW_PUSH_SMALL SW1
U 1 1 530BAED9
P 9900 1750
F 0 "SW1" H 10050 1860 30  0000 C CNN
F 1 "TACTM-64N-F" H 9750 1700 30  0000 C CNN
F 2 "TACTM-64N-F:TACTM-64N-F" H 9650 1600 60  0001 C CNN
F 3 "~" H 9900 1750 60  0000 C CNN
	1    9900 1750
	1    0    0    -1  
$EndComp
Text Label 2600 7250 2    60   ~ 0
RX
Text Label 2600 7150 2    60   ~ 0
TX
Text Label 2600 7350 2    60   ~ 0
GND
Text Label 6250 3700 2    60   ~ 0
RX
Text Label 6250 3800 2    60   ~ 0
TX
$Comp
L GND #PWR04
U 1 1 530BB6BD
P 1750 3750
F 0 "#PWR04" H 1750 3750 30  0001 C CNN
F 1 "GND" H 1750 3680 30  0001 C CNN
F 2 "" H 1750 3750 60  0001 C CNN
F 3 "" H 1750 3750 60  0001 C CNN
	1    1750 3750
	1    0    0    -1  
$EndComp
$Comp
L C-RESCUE-monitor C4
U 1 1 530BB6C3
P 1750 3450
F 0 "C4" V 1800 3550 50  0000 L CNN
F 1 "100nF" V 1800 3150 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 1750 3450 60  0001 C CNN
F 3 "" H 1750 3450 60  0001 C CNN
	1    1750 3450
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR05
U 1 1 530BB6C9
P 1350 3750
F 0 "#PWR05" H 1350 3750 30  0001 C CNN
F 1 "GND" H 1350 3680 30  0001 C CNN
F 2 "" H 1350 3750 60  0001 C CNN
F 3 "" H 1350 3750 60  0001 C CNN
	1    1350 3750
	1    0    0    -1  
$EndComp
$Comp
L R-RESCUE-monitor R2
U 1 1 530BB6CF
P 1350 3450
F 0 "R2" V 1430 3450 50  0000 C CNN
F 1 "20k 2%" V 1250 3450 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 1350 3450 60  0001 C CNN
F 3 "" H 1350 3450 60  0001 C CNN
	1    1350 3450
	1    0    0    -1  
$EndComp
$Comp
L R-RESCUE-monitor R1
U 1 1 530BB6D5
P 1350 2750
F 0 "R1" V 1430 2750 50  0000 C CNN
F 1 "100k 2%" V 1250 2750 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 1350 2750 60  0001 C CNN
F 3 "" H 1350 2750 60  0001 C CNN
	1    1350 2750
	1    0    0    -1  
$EndComp
$Comp
L +BATT #PWR06
U 1 1 530BB6DB
P 1350 2450
F 0 "#PWR06" H 1350 2400 20  0001 C CNN
F 1 "+BATT" H 1350 2550 30  0000 C CNN
F 2 "" H 1350 2450 60  0001 C CNN
F 3 "" H 1350 2450 60  0001 C CNN
	1    1350 2450
	1    0    0    -1  
$EndComp
Text Label 2200 3150 2    60   ~ 0
BATT_DIV
Text Label 4400 2350 3    60   ~ 0
BATT_DIV
Text Label 5300 5650 1    60   ~ 0
SPI_SCK
Text Label 850  4550 0    60   ~ 0
SPI_SCK
Text Label 6250 4300 2    60   ~ 0
SPI_MISO
Text Label 2650 4450 2    60   ~ 0
SPI_MISO
Text Label 5200 5650 1    60   ~ 0
SPI_MOSI
Text Label 850  4450 0    60   ~ 0
SPI_MOSI
Text Label 850  4750 0    60   ~ 0
SPI_nCS
Text Label 6250 4200 2    60   ~ 0
SPI_nCS
$Comp
L VCC #PWR07
U 1 1 531B651F
P 9800 5550
F 0 "#PWR07" H 9800 5650 30  0001 C CNN
F 1 "VCC" H 9800 5650 30  0000 C CNN
F 2 "" H 9800 5550 60  0001 C CNN
F 3 "" H 9800 5550 60  0001 C CNN
	1    9800 5550
	1    0    0    -1  
$EndComp
$Comp
L R-RESCUE-monitor R8
U 1 1 531B6525
P 9350 5950
F 0 "R8" V 9400 5650 50  0000 C CNN
F 1 "330R" V 9350 5950 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 9350 5950 60  0001 C CNN
F 3 "" H 9350 5950 60  0001 C CNN
	1    9350 5950
	0    -1   -1   0   
$EndComp
$Comp
L R-RESCUE-monitor R7
U 1 1 531B652B
P 9350 5850
F 0 "R7" V 9400 5550 50  0000 C CNN
F 1 "330R" V 9350 5850 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 9350 5850 60  0001 C CNN
F 3 "" H 9350 5850 60  0001 C CNN
	1    9350 5850
	0    -1   -1   0   
$EndComp
$Comp
L R-RESCUE-monitor R6
U 1 1 531B6531
P 9350 5650
F 0 "R6" V 9400 5350 50  0000 C CNN
F 1 "330R" V 9350 5650 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 9350 5650 60  0001 C CNN
F 3 "" H 9350 5650 60  0001 C CNN
	1    9350 5650
	0    -1   -1   0   
$EndComp
$Comp
L RGB_LED L2
U 1 1 531B6537
P 9950 5750
F 0 "L2" H 10100 5400 60  0000 C CNN
F 1 "HB5-40ARAAGCABC" V 10550 5650 60  0000 C CNN
F 2 "LEDs:LED-RGB-5MM_Common_Cathode" H 9950 5750 60  0001 C CNN
F 3 "" H 9950 5750 60  0001 C CNN
	1    9950 5750
	1    0    0    -1  
$EndComp
Text Label 8650 5650 0    60   ~ 0
~LED_R
Text Label 8650 5850 0    60   ~ 0
~LED_G
Text Label 8650 5950 0    60   ~ 0
~LED_B
Text Label 6200 5800 0    60   ~ 0
~LED_ALRM
Text Label 5200 2350 3    60   ~ 0
~LED_R
Text Label 5100 2350 3    60   ~ 0
~LED_G
Text Label 5000 2350 3    60   ~ 0
~LED_B
Text Label 3300 2350 3    60   ~ 0
GND
Text Label 3500 2350 3    60   ~ 0
GND
Text Label 4000 2350 3    60   ~ 0
GND
Text Label 3200 5650 1    60   ~ 0
GND
Text Label 3400 2350 3    60   ~ 0
VCC
Text Label 3300 5650 1    60   ~ 0
VCC
Text Label 5300 2350 3    60   ~ 0
GND
$Comp
L C-RESCUE-monitor C6
U 1 1 53246A3D
P 7900 1350
F 0 "C6" H 7900 1450 40  0000 L CNN
F 1 "100nF" H 7906 1265 40  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 7938 1200 30  0001 C CNN
F 3 "~" H 7900 1350 60  0000 C CNN
	1    7900 1350
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR08
U 1 1 53246DDC
P 8100 1700
F 0 "#PWR08" H 8100 1700 30  0001 C CNN
F 1 "GND" H 8100 1630 30  0001 C CNN
F 2 "" H 8100 1700 60  0001 C CNN
F 3 "" H 8100 1700 60  0001 C CNN
	1    8100 1700
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR09
U 1 1 53246DEB
P 8100 950
F 0 "#PWR09" H 8100 1050 30  0001 C CNN
F 1 "VCC" H 8100 1050 30  0000 C CNN
F 2 "" H 8100 950 60  0001 C CNN
F 3 "" H 8100 950 60  0001 C CNN
	1    8100 950 
	1    0    0    -1  
$EndComp
Text Notes 5400 7500 0    60   ~ 0
ZL322-2X5P
$Comp
L CP1-RESCUE-monitor C8
U 1 1 53249D18
P 8350 1350
F 0 "C8" H 8400 1450 50  0000 L CNN
F 1 "10uF/6V" V 8400 1750 50  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:TantalC_SizeA_EIA-3216_HandSoldering" H 8350 1350 60  0001 C CNN
F 3 "" H 8350 1350 60  0001 C CNN
	1    8350 1350
	1    0    0    -1  
$EndComp
Text Label 6250 3900 2    60   ~ 0
~LED_ALRM
Text Notes 5000 2250 1    60   ~ 0
PWM
Text Notes 5100 2250 1    60   ~ 0
PWM
Text Notes 5200 2250 1    60   ~ 0
PWM
Text Label 5000 5650 1    60   ~ 0
SPI_nSDCS
NoConn ~ 2050 5300
NoConn ~ 2050 6000
Text Label 2650 6200 2    60   ~ 0
GND
Text Label 4900 5650 1    60   ~ 0
CARD_INSERT
Text Label 2650 6100 2    60   ~ 0
CARD_INSERT
Wire Wire Line
	1200 6550 1200 6850
Connection ~ 1200 6750
Wire Wire Line
	1000 6750 1300 6750
Wire Wire Line
	1000 6750 1000 6550
Wire Wire Line
	2650 5900 2050 5900
Wire Wire Line
	2650 5800 2050 5800
Wire Wire Line
	2650 5700 2050 5700
Wire Wire Line
	2650 5600 2050 5600
Wire Wire Line
	2650 5500 2050 5500
Wire Wire Line
	2650 5400 2050 5400
Wire Wire Line
	4850 7050 5550 7050
Wire Wire Line
	4850 6850 5550 6850
Wire Wire Line
	6350 7250 5800 7250
Wire Wire Line
	6350 7050 5800 7050
Wire Wire Line
	6350 6950 5800 6950
Wire Wire Line
	6350 6850 5800 6850
Wire Wire Line
	4850 6950 5550 6950
Wire Wire Line
	4850 7250 5550 7250
Wire Wire Line
	9800 750  9800 850 
Wire Wire Line
	9800 2000 9800 2100
Wire Wire Line
	9550 1450 10150 1450
Connection ~ 9650 1450
Wire Wire Line
	9800 1350 9800 1650
Connection ~ 9800 1450
Wire Wire Line
	9250 750  9800 750 
Wire Wire Line
	9250 750  9250 1100
Wire Wire Line
	9250 1800 9250 2000
Wire Wire Line
	9250 2000 10000 2000
Connection ~ 9800 2000
Wire Wire Line
	10000 2000 10000 1850
Wire Wire Line
	2600 7250 2200 7250
Wire Wire Line
	2600 7350 2200 7350
Wire Wire Line
	2200 7150 2600 7150
Wire Wire Line
	1750 3650 1750 3750
Wire Wire Line
	1350 3700 1350 3750
Wire Wire Line
	1350 3000 1350 3200
Wire Wire Line
	1350 2450 1350 2500
Connection ~ 1350 3150
Wire Wire Line
	1750 3250 1750 3150
Connection ~ 1750 3150
Wire Wire Line
	1350 3150 2200 3150
Wire Wire Line
	1300 4750 850  4750
Wire Wire Line
	850  4450 1300 4450
Wire Wire Line
	2200 4450 2650 4450
Wire Wire Line
	850  4550 1300 4550
Wire Wire Line
	5300 2350 5300 2900
Wire Wire Line
	5200 2350 5200 2900
Wire Wire Line
	5100 2350 5100 2900
Wire Wire Line
	5000 2350 5000 2900
Wire Wire Line
	4700 2350 4700 2900
Wire Wire Line
	4500 2350 4500 2900
Wire Wire Line
	4400 2350 4400 2900
Wire Wire Line
	4300 2350 4300 2900
Wire Wire Line
	4200 2350 4200 2900
Wire Wire Line
	4100 2350 4100 2900
Wire Wire Line
	4000 2350 4000 2900
Wire Wire Line
	3900 2350 3900 2900
Wire Wire Line
	3800 2350 3800 2900
Wire Wire Line
	3700 2350 3700 2900
Wire Wire Line
	3600 2350 3600 2900
Wire Wire Line
	3500 2350 3500 2900
Wire Wire Line
	3400 2350 3400 2900
Wire Wire Line
	3300 2350 3300 2900
Wire Wire Line
	3700 5100 3700 5650
Wire Wire Line
	3600 5100 3600 5650
Wire Wire Line
	3500 5100 3500 5650
Wire Wire Line
	3400 5100 3400 5650
Wire Wire Line
	3300 5100 3300 5650
Wire Wire Line
	3200 5100 3200 5650
Wire Wire Line
	4100 5100 4100 5650
Wire Wire Line
	4000 5100 4000 5650
Wire Wire Line
	3900 5100 3900 5650
Wire Wire Line
	3800 5100 3800 5650
Wire Wire Line
	5000 5100 5000 5650
Wire Wire Line
	5200 5100 5200 5650
Wire Wire Line
	5100 5100 5100 5650
Wire Wire Line
	5300 5100 5300 5650
Wire Wire Line
	5700 3900 6250 3900
Wire Wire Line
	5700 3700 6250 3700
Wire Wire Line
	5700 3800 6250 3800
Wire Wire Line
	5700 3600 6250 3600
Wire Wire Line
	5700 4300 6250 4300
Wire Wire Line
	5700 4100 6250 4100
Wire Wire Line
	5700 4200 6250 4200
Wire Wire Line
	5700 4000 6250 4000
Wire Wire Line
	9950 5850 9600 5850
Wire Wire Line
	9950 5650 9600 5650
Wire Wire Line
	9950 5950 9600 5950
Wire Wire Line
	9950 5750 9800 5750
Wire Wire Line
	8650 5650 9100 5650
Wire Wire Line
	8650 5850 9100 5850
Wire Wire Line
	8650 5950 9100 5950
Wire Wire Line
	8350 1650 8350 1550
Wire Wire Line
	7900 1650 7900 1550
Wire Wire Line
	8100 1650 8100 1700
Connection ~ 8100 1650
Wire Wire Line
	8350 1050 8350 1150
Wire Wire Line
	7900 1050 7900 1150
Wire Wire Line
	8100 1050 8100 950 
Connection ~ 8100 1050
Wire Wire Line
	1100 6550 1100 6750
Connection ~ 1100 6750
Wire Wire Line
	1300 6750 1300 6550
Wire Wire Line
	2650 6100 2050 6100
Wire Wire Line
	2650 6200 2050 6200
Wire Wire Line
	4200 5100 4200 5650
Wire Wire Line
	4300 5100 4300 5650
Wire Wire Line
	4400 5100 4400 5650
Wire Wire Line
	4600 5100 4600 5650
Wire Wire Line
	4700 5100 4700 5650
Wire Wire Line
	4800 5100 4800 5650
Wire Wire Line
	4900 5100 4900 5650
Text Label 4700 5650 1    60   ~ 0
SCL
Text Label 4800 5650 1    60   ~ 0
SDA
Text Label 3400 5650 1    60   ~ 0
GND
Text Notes 7050 7050 0    60   ~ 0
O modulach BTM-222\nhttp://www.elektroda.pl/rtvforum/topic1433390.html
$Comp
L AT45DB161E-SHD-B U1
U 1 1 558FE78B
P 1750 4950
F 0 "U1" H 1650 5550 60  0000 C CNN
F 1 "AT45DB161E-SHD-B" H 1750 5050 60  0000 C CNN
F 2 "Housings_SOIC:SOIJ-8_5.3x5.3mm_Pitch1.27mm" H 1750 4950 60  0001 C CNN
F 3 "" H 1750 4950 60  0000 C CNN
	1    1750 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 4550 2650 4550
Wire Wire Line
	2200 4650 2650 4650
Wire Wire Line
	2200 4750 2650 4750
Wire Wire Line
	850  4650 1300 4650
Text Label 2650 4550 2    60   ~ 0
GND
Text Label 2650 4650 2    60   ~ 0
VCC
Text Label 2650 4750 2    60   ~ 0
VCC
Text Label 850  4650 0    60   ~ 0
VCC
Text Notes 1600 5000 0    60   ~ 0
2MB
$Comp
L HC-05 U6
U 1 1 559023D8
P 7050 2850
F 0 "U6" H 7350 3150 60  0000 C CNN
F 1 "HC-05" H 7450 3050 60  0000 C CNN
F 2 "monitor:HC-05" H 7550 2300 60  0001 C CNN
F 3 "" H 7550 2300 60  0000 C CNN
	1    7050 2850
	1    0    0    -1  
$EndComp
$Comp
L DERFMEGA256-23M00 U3
U 1 1 558FD80D
P 3200 5100
F 0 "U3" H 3800 6050 60  0000 C CNN
F 1 "DERFMEGA256-23M00" H 3850 6150 60  0000 C CNN
F 2 "monitor:DERFMEGA256-23M00" H 3800 6450 60  0001 C CNN
F 3 "" H 3800 6450 60  0000 C CNN
	1    3200 5100
	1    0    0    -1  
$EndComp
Text Notes 6300 4100 0    60   ~ 0
PWM
Wire Wire Line
	6650 2850 7050 2850
Wire Wire Line
	6650 2950 7050 2950
Text Label 4300 5650 1    60   ~ 0
BT_RXD
Text Label 6650 2950 0    60   ~ 0
BT_RXD
Text Label 4400 5650 1    60   ~ 0
BT_TXD
Text Label 6650 2850 0    60   ~ 0
BT_TXD
Wire Wire Line
	6650 3950 7050 3950
Wire Wire Line
	6650 4050 7050 4050
Text Label 6650 3950 0    60   ~ 0
BT_VCC
Text Label 6650 4050 0    60   ~ 0
GND
Text Label 4700 2350 3    60   ~ 0
~EN_BT_VCC
Wire Wire Line
	6650 3850 7050 3850
Wire Wire Line
	8450 4050 8850 4050
Text Label 8850 4050 2    60   ~ 0
GND
Wire Wire Line
	8100 5000 8100 4600
Text Label 8100 5000 1    60   ~ 0
GND
Text Notes 700  3450 1    50   ~ 0
Lithium Polymer Battery Pack (LP903450) 1600mAh 3.7V with\nProtection Circuit Module (PCM)
Text Label 3600 5650 1    60   ~ 0
~RST_OUT
Text Label 6650 3850 0    60   ~ 0
~RST_OUT
Wire Wire Line
	8900 2850 8450 2850
Wire Wire Line
	8900 3050 8450 3050
Text Label 8900 2850 2    60   ~ 0
BT_KEY
Text Label 8900 3050 2    60   ~ 0
BT_PAIRED
Text Label 8900 3150 2    60   ~ 0
BT_LED
Wire Wire Line
	8900 3150 8450 3150
$Comp
L MCP73833 U2
U 1 1 5592F913
P 2850 900
F 0 "U2" H 3350 100 60  0000 C CNN
F 1 "MCP73833-FCI/UN" H 3300 1000 60  0000 C CNN
F 2 "Housings_SSOP:MSOP-10_3x3mm_Pitch0.5mm" H 3300 1000 60  0001 C CNN
F 3 "" H 3300 1000 60  0000 C CNN
	1    2850 900 
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X02 P1
U 1 1 55942CC8
P 850 950
F 0 "P1" H 850 1100 50  0000 C CNN
F 1 "QI receiver" V 950 950 50  0000 C CNN
F 2 "Connect:PINHEAD1-2" H 850 950 60  0001 C CNN
F 3 "" H 850 950 60  0000 C CNN
	1    850  950 
	-1   0    0    1   
$EndComp
Wire Wire Line
	1050 900  2850 900 
Wire Wire Line
	2850 1000 2750 1000
Wire Wire Line
	2750 1000 2750 900 
Connection ~ 2750 900 
$Comp
L CAPAPOL C2
U 1 1 559431BA
P 1700 1400
F 0 "C2" V 1650 1550 50  0000 L CNN
F 1 "22uF/10V" V 1550 1200 50  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:TantalC_SizeB_EIA-3528_HandSoldering" H 1800 1250 30  0001 C CNN
F 3 "T491B226K010AT" H 1700 1400 300 0001 C CNN
	1    1700 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 1800 1700 1600
Wire Wire Line
	1700 1200 1700 900 
Connection ~ 1700 900 
Wire Wire Line
	3850 900  5100 900 
Wire Wire Line
	3850 1000 3950 1000
Wire Wire Line
	3950 1000 3950 900 
Connection ~ 3950 900 
Wire Wire Line
	4800 1800 4800 1600
$Comp
L R-RESCUE-monitor R11
U 1 1 55943D4A
P 2500 1400
F 0 "R11" V 2400 1300 50  0000 C CNN
F 1 "330R" V 2507 1401 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 2430 1400 30  0001 C CNN
F 3 "" H 2500 1400 30  0000 C CNN
	1    2500 1400
	0    1    1    0   
$EndComp
$Comp
L R-RESCUE-monitor R10
U 1 1 55943EAA
P 2400 1500
F 0 "R10" V 2350 1300 50  0000 C CNN
F 1 "330R" V 2407 1501 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 2330 1500 30  0001 C CNN
F 3 "" H 2400 1500 30  0000 C CNN
	1    2400 1500
	0    1    1    0   
$EndComp
$Comp
L R-RESCUE-monitor R9
U 1 1 55943EE9
P 2300 1600
F 0 "R9" V 2250 1400 50  0000 C CNN
F 1 "330R" V 2307 1601 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 2230 1600 30  0001 C CNN
F 3 "" H 2300 1600 30  0000 C CNN
	1    2300 1600
	0    1    1    0   
$EndComp
$Comp
L LED-RESCUE-monitor D3
U 1 1 55944010
P 2200 1150
F 0 "D3" H 2050 1200 50  0000 C CNN
F 1 "LED" H 2350 1200 50  0000 C CNN
F 2 "LEDs:LED-0805" H 2200 1150 60  0001 C CNN
F 3 "" H 2200 1150 60  0000 C CNN
	1    2200 1150
	0    1    1    0   
$EndComp
$Comp
L LED-RESCUE-monitor D2
U 1 1 559441AB
P 2050 1150
F 0 "D2" H 1900 1200 50  0000 C CNN
F 1 "LED" H 2200 1200 50  0000 C CNN
F 2 "LEDs:LED-0805" H 2050 1150 60  0001 C CNN
F 3 "" H 2050 1150 60  0000 C CNN
	1    2050 1150
	0    1    1    0   
$EndComp
$Comp
L LED-RESCUE-monitor D1
U 1 1 559441EF
P 1900 1150
F 0 "D1" H 1750 1200 50  0000 C CNN
F 1 "LED" H 2050 1200 50  0000 C CNN
F 2 "LEDs:LED-0805" H 1900 1150 60  0001 C CNN
F 3 "" H 1900 1150 60  0000 C CNN
	1    1900 1150
	0    1    1    0   
$EndComp
Wire Wire Line
	1900 950  1900 900 
Connection ~ 1900 900 
Wire Wire Line
	2050 950  2050 900 
Connection ~ 2050 900 
Wire Wire Line
	2200 950  2200 900 
Connection ~ 2200 900 
Wire Wire Line
	2200 1350 2200 1400
Wire Wire Line
	2200 1400 2250 1400
Wire Wire Line
	2050 1350 2050 1500
Wire Wire Line
	2050 1500 2150 1500
Wire Wire Line
	1900 1350 1900 1600
Wire Wire Line
	1900 1600 2050 1600
Wire Wire Line
	2750 1400 2850 1400
Wire Wire Line
	2650 1500 2850 1500
Wire Wire Line
	2550 1600 2850 1600
$Comp
L R-RESCUE-monitor R15
U 1 1 55944E05
P 4100 1450
F 0 "R15" V 4180 1450 50  0000 C CNN
F 1 "1k2" V 4107 1451 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 4030 1450 30  0001 C CNN
F 3 "" H 4100 1450 30  0000 C CNN
	1    4100 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 1600 3900 1600
Wire Wire Line
	3900 1600 3900 1800
Wire Wire Line
	4100 1800 4100 1700
Wire Wire Line
	4100 1200 3850 1200
Wire Wire Line
	1050 1000 1350 1000
Wire Wire Line
	1350 1000 1350 1800
Text Notes 1900 850  1    30   ~ 0
QI POWER - yellow
Text Notes 2200 850  1    30   ~ 0
CHRG PROG red
Text Notes 2050 850  1    30   ~ 0
CHRG COMPL green
$Comp
L +BATT #PWR010
U 1 1 55946304
P 4450 850
F 0 "#PWR010" H 4450 800 20  0001 C CNN
F 1 "+BATT" H 4450 950 30  0000 C CNN
F 2 "" H 4450 850 60  0001 C CNN
F 3 "" H 4450 850 60  0001 C CNN
	1    4450 850 
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 900  4450 850 
Connection ~ 4450 900 
$Comp
L THERMISTOR TH1
U 1 1 55946C9B
P 4500 1450
F 0 "TH1" V 4550 1850 50  0000 C CNN
F 1 "NTCM-HP-10K-1%" V 4400 1400 50  0000 C BNN
F 2 "Connect:PINHEAD1-2" H 4500 1450 60  0001 C CNN
F 3 "" H 4500 1450 60  0000 C CNN
	1    4500 1450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 1100 4500 1100
Wire Wire Line
	4500 1100 4500 1200
Wire Wire Line
	4500 1800 4500 1700
$Comp
L BATTERY BT1
U 1 1 55906AFD
P 4800 1300
F 0 "BT1" H 5000 1400 50  0000 C CNN
F 1 "LP903450" H 4800 1110 50  0000 C CNN
F 2 "Connect:PINHEAD1-2" H 4800 1300 60  0001 C CNN
F 3 "" H 4800 1300 60  0000 C CNN
	1    4800 1300
	0    1    1    0   
$EndComp
Wire Wire Line
	4800 900  4800 1000
Wire Wire Line
	2600 900  2600 700 
Connection ~ 4800 900 
$Comp
L DIODESCH D4
U 1 1 55947CFB
P 5300 700
F 0 "D4" H 5100 750 50  0000 C CNN
F 1 "SK210" H 5300 800 50  0000 C CNN
F 2 "Diodes_SMD:Diode-SMB_Handsoldering" H 5300 700 60  0001 C CNN
F 3 "" H 5300 700 60  0000 C CNN
	1    5300 700 
	1    0    0    -1  
$EndComp
$Comp
L DIODESCH D5
U 1 1 55947DDC
P 5300 900
F 0 "D5" H 5100 950 50  0000 C CNN
F 1 "SK210" H 5300 800 50  0000 C CNN
F 2 "Diodes_SMD:Diode-SMB_Handsoldering" H 5300 900 60  0001 C CNN
F 3 "" H 5300 900 60  0000 C CNN
	1    5300 900 
	1    0    0    -1  
$EndComp
$Comp
L APE8865Y5-33-HF-3 U4
U 1 1 55949491
P 6450 950
F 0 "U4" H 6150 1200 40  0000 C CNN
F 1 "APE8865Y5-33-HF-3" H 6450 1150 40  0000 C CNN
F 2 "Housings_SOT-23_SOT-143_TSOT-6:SOT-23-5" H 6450 1050 35  0001 C CIN
F 3 "" H 6450 950 60  0000 C CNN
	1    6450 950 
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 700  5100 700 
$Comp
L C-RESCUE-monitor C5
U 1 1 5594A3D6
P 7000 1350
F 0 "C5" V 6950 1450 50  0000 L CNN
F 1 "10nF" V 6950 1100 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 7038 1200 30  0001 C CNN
F 3 "" H 7000 1350 60  0000 C CNN
	1    7000 1350
	1    0    0    -1  
$EndComp
$Comp
L CAPAPOL C3
U 1 1 5594A523
P 5700 1350
F 0 "C3" H 5750 1450 50  0000 L CNN
F 1 "22uF/tnt" H 5750 1250 50  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:TantalC_SizeB_EIA-3528_HandSoldering" H 5800 1200 30  0001 C CNN
F 3 "T491B226K010AT" H 5700 1350 300 0001 C CNN
	1    5700 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 900  6000 900 
Wire Wire Line
	5500 700  5600 700 
Wire Wire Line
	5600 700  5600 900 
Connection ~ 5600 900 
Wire Wire Line
	5700 900  5700 1150
Connection ~ 5700 900 
Wire Wire Line
	6000 1050 5900 1050
Wire Wire Line
	5900 1050 5900 900 
Connection ~ 5900 900 
Wire Wire Line
	6900 1050 7000 1050
Wire Wire Line
	7000 1050 7000 1150
Wire Wire Line
	6450 1250 6450 1850
$Comp
L GND #PWR011
U 1 1 5594B166
P 6450 1850
F 0 "#PWR011" H 6450 1600 60  0001 C CNN
F 1 "GND" H 6450 1700 60  0000 C CNN
F 2 "" H 6450 1850 60  0000 C CNN
F 3 "" H 6450 1850 60  0000 C CNN
	1    6450 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 1550 5700 1800
Wire Wire Line
	1350 1800 7650 1800
Connection ~ 6450 1800
Wire Wire Line
	7000 1800 7000 1550
$Comp
L CAPAPOL C9
U 1 1 5594B8E2
P 7400 1350
F 0 "C9" V 7450 1450 50  0000 L CNN
F 1 "22uF/tnt" V 7350 1450 50  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:TantalC_SizeB_EIA-3528_HandSoldering" H 7500 1200 30  0001 C CNN
F 3 "" H 7400 1350 300 0000 C CNN
	1    7400 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	7400 1800 7400 1550
Connection ~ 7000 1800
Wire Wire Line
	7400 900  7400 1150
Wire Wire Line
	6900 900  7650 900 
Wire Wire Line
	9550 700  9550 750 
Connection ~ 9550 750 
$Comp
L VCC #PWR012
U 1 1 5594BE68
P 7300 850
F 0 "#PWR012" H 7300 950 30  0001 C CNN
F 1 "VCC" H 7300 950 30  0000 C CNN
F 2 "" H 7300 850 60  0001 C CNN
F 3 "" H 7300 850 60  0001 C CNN
	1    7300 850 
	1    0    0    -1  
$EndComp
Wire Wire Line
	7300 850  7300 900 
Connection ~ 7300 900 
$Comp
L CAPAPOL C10
U 1 1 559ACD03
P 7650 1350
F 0 "C10" V 7700 1450 50  0000 L CNN
F 1 "220uF/6.3V" V 7600 1450 50  0000 L CNN
F 2 "Capacitors_SMD:c_elec_6.3x5.8" H 7750 1200 30  0001 C CNN
F 3 "UWT0J221MCL6GS" H 7650 1350 300 0001 C CNN
	1    7650 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	7650 1800 7650 1550
Connection ~ 7400 1800
Wire Wire Line
	7650 900  7650 1150
Connection ~ 7400 900 
Connection ~ 4800 1800
Connection ~ 4500 1800
Connection ~ 4100 1800
Connection ~ 3900 1800
Connection ~ 1700 1800
Text Label 1400 1800 0    60   ~ 0
GND
Wire Wire Line
	2800 1400 2800 1850
Connection ~ 2800 1400
Wire Wire Line
	2700 1500 2700 1850
Connection ~ 2700 1500
Wire Wire Line
	2600 1600 2600 1850
Connection ~ 2600 1600
Text Label 2600 2900 1    60   ~ 0
~QI_POWER
Text Label 2700 2900 1    60   ~ 0
~CHRG_CMPL
Text Label 2800 2900 1    60   ~ 0
~CHRG_PROG
$Comp
L R-RESCUE-monitor R3
U 1 1 559B0689
P 1100 2750
F 0 "R3" V 1180 2750 50  0000 C CNN
F 1 "100k 2%" V 1000 2750 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 1100 2750 60  0001 C CNN
F 3 "" H 1100 2750 60  0001 C CNN
	1    1100 2750
	1    0    0    -1  
$EndComp
$Comp
L R-RESCUE-monitor R4
U 1 1 559B06D1
P 1100 3450
F 0 "R4" V 1180 3450 50  0000 C CNN
F 1 "20k 2%" V 1000 3450 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 1100 3450 60  0001 C CNN
F 3 "" H 1100 3450 60  0001 C CNN
	1    1100 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	1100 3000 1100 3200
Wire Wire Line
	1100 3700 1100 3750
Wire Wire Line
	1100 2500 1100 2450
$Comp
L GND #PWR013
U 1 1 559B0C47
P 1100 3750
F 0 "#PWR013" H 1100 3750 30  0001 C CNN
F 1 "GND" H 1100 3680 30  0001 C CNN
F 2 "" H 1100 3750 60  0001 C CNN
F 3 "" H 1100 3750 60  0001 C CNN
	1    1100 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	1100 3050 2200 3050
Connection ~ 1100 3050
Text Label 2200 3050 2    60   ~ 0
QI_VOLT
Text Label 3700 5650 1    60   ~ 0
~QI_POWER
Text Label 3800 5650 1    60   ~ 0
~CHRG_CMPL
Text Label 3900 5650 1    60   ~ 0
~CHRG_PROG
$Comp
L +5C #PWR014
U 1 1 559B1A26
P 1350 850
F 0 "#PWR014" H 1350 700 60  0001 C CNN
F 1 "+5C" H 1350 990 60  0000 C CNN
F 2 "" H 1350 850 60  0000 C CNN
F 3 "" H 1350 850 60  0000 C CNN
	1    1350 850 
	1    0    0    -1  
$EndComp
$Comp
L +5C #PWR015
U 1 1 559B1BB0
P 1100 2450
F 0 "#PWR015" H 1100 2300 60  0001 C CNN
F 1 "+5C" H 1100 2590 60  0000 C CNN
F 2 "" H 1100 2450 60  0000 C CNN
F 3 "" H 1100 2450 60  0000 C CNN
	1    1100 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	1350 850  1350 900 
Connection ~ 1350 900 
Text Label 4300 2350 3    60   ~ 0
QI_VOLT
$Comp
L GND #PWR016
U 1 1 559B285C
P 1550 3750
F 0 "#PWR016" H 1550 3750 30  0001 C CNN
F 1 "GND" H 1550 3680 30  0001 C CNN
F 2 "" H 1550 3750 60  0001 C CNN
F 3 "" H 1550 3750 60  0001 C CNN
	1    1550 3750
	1    0    0    -1  
$EndComp
$Comp
L C-RESCUE-monitor C1
U 1 1 559B2862
P 1550 3450
F 0 "C1" V 1600 3550 50  0000 L CNN
F 1 "100nF" V 1600 3150 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 1550 3450 60  0001 C CNN
F 3 "" H 1550 3450 60  0001 C CNN
	1    1550 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	1550 3650 1550 3750
Wire Wire Line
	1550 3250 1550 3050
Connection ~ 1550 3050
Wire Wire Line
	3850 6950 3350 6950
Wire Wire Line
	3850 7050 3350 7050
Wire Wire Line
	3850 7150 3350 7150
Wire Wire Line
	3850 7250 3350 7250
Wire Wire Line
	3850 7350 3350 7350
Text Label 3850 6750 2    60   ~ 0
GND
Text Label 3850 6550 2    60   ~ 0
SCL
Text Label 3850 6650 2    60   ~ 0
SDA
Text Label 3850 6950 2    60   ~ 0
VCC
Text Label 3850 6350 2    60   ~ 0
KEYB_IRQ
Text Label 4200 5650 1    60   ~ 0
KEYB_IRQ
Text Label 10150 1450 2    60   ~ 0
~RST
$Comp
L VCC #PWR017
U 1 1 5590557C
P 6400 5400
F 0 "#PWR017" H 6400 5250 60  0001 C CNN
F 1 "VCC" H 6400 5550 60  0000 C CNN
F 2 "" H 6400 5400 60  0000 C CNN
F 3 "" H 6400 5400 60  0000 C CNN
	1    6400 5400
	0    -1   -1   0   
$EndComp
$Comp
L R-RESCUE-monitor R18
U 1 1 55905027
P 7650 1950
F 0 "R18" V 7730 1950 50  0000 C CNN
F 1 "0R" V 7657 1951 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 7580 1950 30  0001 C CNN
F 3 "" H 7650 1950 30  0000 C CNN
	1    7650 1950
	0    1    1    0   
$EndComp
$Comp
L IRLM6302PBF Q1
U 1 1 55901ABA
P 6850 5500
F 0 "Q1" V 7150 5500 60  0000 R CNN
F 1 "IRLML6302PBF" V 7050 5800 60  0000 R CNN
F 2 "Housings_SOT-23_SOT-143_TSOT-6:SOT-23_Handsoldering" H 6850 5500 60  0001 C CNN
F 3 "" H 6850 5500 60  0000 C CNN
	1    6850 5500
	0    1    -1   0   
$EndComp
$Comp
L IRLM6302PBF Q2
U 1 1 559C2963
P 7650 2250
F 0 "Q2" V 7600 2550 60  0000 R CNN
F 1 "IRLML6302PBF" V 7500 3000 60  0000 R CNN
F 2 "Housings_SOT-23_SOT-143_TSOT-6:SOT-23_Handsoldering" H 7650 2250 60  0001 C CNN
F 3 "" H 7650 2250 60  0000 C CNN
	1    7650 2250
	0    1    -1   0   
$EndComp
Text Label 6650 2500 0    60   ~ 0
~EN_BT_VCC
Wire Wire Line
	7650 2450 7650 2500
Wire Wire Line
	7650 2500 6650 2500
$Comp
L VCC #PWR018
U 1 1 559C324F
P 7150 2150
F 0 "#PWR018" H 7150 2000 60  0001 C CNN
F 1 "VCC" H 7150 2300 60  0000 C CNN
F 2 "" H 7150 2150 60  0000 C CNN
F 3 "" H 7150 2150 60  0000 C CNN
	1    7150 2150
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7150 2150 7450 2150
Wire Wire Line
	7400 1950 7300 1950
Wire Wire Line
	7300 1950 7300 2150
Connection ~ 7300 2150
Wire Wire Line
	7850 2150 8550 2150
Wire Wire Line
	7900 1950 8000 1950
Wire Wire Line
	8000 1950 8000 2150
Connection ~ 8000 2150
Text Label 8400 2150 2    60   ~ 0
BT_VCC
Wire Wire Line
	6650 5400 6400 5400
Wire Wire Line
	6850 5700 6850 5800
Wire Wire Line
	6850 5800 6200 5800
$Comp
L R-RESCUE-monitor R16
U 1 1 559C5065
P 7450 5400
F 0 "R16" V 7500 5150 50  0000 C CNN
F 1 "330R" V 7450 5400 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 7450 5400 60  0001 C CNN
F 3 "" H 7450 5400 60  0001 C CNN
	1    7450 5400
	0    -1   -1   0   
$EndComp
$Comp
L R-RESCUE-monitor R17
U 1 1 559C510C
P 7450 5550
F 0 "R17" V 7500 5300 50  0000 C CNN
F 1 "330R" V 7450 5550 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 7450 5550 60  0001 C CNN
F 3 "" H 7450 5550 60  0001 C CNN
	1    7450 5550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7050 5400 7200 5400
Wire Wire Line
	7100 5400 7100 5550
Wire Wire Line
	7100 5550 7200 5550
Connection ~ 7100 5400
$Comp
L LED-RESCUE-monitor D6
U 1 1 559C5657
P 8050 5400
F 0 "D6" H 7850 5350 50  0000 C CNN
F 1 "OS5RKA5111P" H 8050 5500 50  0000 C CNN
F 2 "LEDs:LED-5MM" H 8050 5400 60  0001 C CNN
F 3 "" H 8050 5400 60  0001 C CNN
	1    8050 5400
	1    0    0    -1  
$EndComp
$Comp
L LED-RESCUE-monitor D7
U 1 1 559C5776
P 8050 5550
F 0 "D7" H 7850 5500 50  0000 C CNN
F 1 "OS5RKA5111P" H 8050 5400 50  0000 C CNN
F 2 "LEDs:LED-5MM" H 8050 5550 60  0001 C CNN
F 3 "" H 8050 5550 60  0001 C CNN
	1    8050 5550
	1    0    0    -1  
$EndComp
Wire Wire Line
	7850 5400 7700 5400
Wire Wire Line
	7700 5550 7850 5550
Wire Wire Line
	8250 5400 8400 5400
Wire Wire Line
	8400 5400 8400 5750
Wire Wire Line
	8250 5550 8400 5550
Connection ~ 8400 5550
$Comp
L GND #PWR019
U 1 1 559C5E20
P 8400 5750
F 0 "#PWR019" H 8400 5500 60  0001 C CNN
F 1 "GND" H 8400 5600 60  0000 C CNN
F 2 "" H 8400 5750 60  0000 C CNN
F 3 "" H 8400 5750 60  0000 C CNN
	1    8400 5750
	1    0    0    -1  
$EndComp
Wire Wire Line
	9800 5750 9800 5550
Text Notes 7050 6850 0    60   ~ 0
mozliwosc podlaczenia wyswietlacza OLED REX012864BYPP3N0
Text Label 6250 3600 2    60   ~ 0
BT_KEY
Text Label 4200 2350 3    60   ~ 0
BT_PAIRED
$Comp
L LED-RESCUE-monitor D8
U 1 1 559E4E1B
P 10100 5100
F 0 "D8" H 9900 5050 50  0000 C CNN
F 1 "LED" H 10100 5200 50  0000 C CNN
F 2 "LEDs:LED-0805" H 10100 5100 60  0001 C CNN
F 3 "" H 10100 5100 60  0001 C CNN
	1    10100 5100
	1    0    0    -1  
$EndComp
$Comp
L R-RESCUE-monitor R19
U 1 1 559E5033
P 9350 5100
F 0 "R19" V 9400 4800 50  0000 C CNN
F 1 "330R" V 9350 5100 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 9350 5100 60  0001 C CNN
F 3 "" H 9350 5100 60  0001 C CNN
	1    9350 5100
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8900 3150 8900 5100
Wire Wire Line
	8900 5100 9100 5100
Wire Wire Line
	9600 5100 9900 5100
Wire Wire Line
	10300 5100 10450 5100
Wire Wire Line
	10450 5100 10450 5150
$Comp
L GND #PWR020
U 1 1 559E57EF
P 10450 5150
F 0 "#PWR020" H 10450 4900 60  0001 C CNN
F 1 "GND" H 10450 5000 60  0000 C CNN
F 2 "" H 10450 5150 60  0000 C CNN
F 3 "" H 10450 5150 60  0000 C CNN
	1    10450 5150
	1    0    0    -1  
$EndComp
$Comp
L R-RESCUE-monitor R14
U 1 1 559E604D
P 2800 2100
F 0 "R14" V 2750 1900 50  0000 C CNN
F 1 "4k7" V 2807 2101 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 2730 2100 30  0001 C CNN
F 3 "" H 2800 2100 30  0000 C CNN
	1    2800 2100
	-1   0    0    1   
$EndComp
$Comp
L R-RESCUE-monitor R13
U 1 1 559E6315
P 2700 2100
F 0 "R13" V 2650 1900 50  0000 C CNN
F 1 "4k7" V 2707 2101 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 2630 2100 30  0001 C CNN
F 3 "" H 2700 2100 30  0000 C CNN
	1    2700 2100
	-1   0    0    1   
$EndComp
$Comp
L R-RESCUE-monitor R12
U 1 1 559E63A7
P 2600 2100
F 0 "R12" V 2550 1900 50  0000 C CNN
F 1 "4k7" V 2607 2101 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 2530 2100 30  0001 C CNN
F 3 "" H 2600 2100 30  0000 C CNN
	1    2600 2100
	-1   0    0    1   
$EndComp
Wire Wire Line
	2800 2900 2800 2350
Wire Wire Line
	2700 2900 2700 2350
Wire Wire Line
	2600 2900 2600 2350
Text Label 4000 5650 1    60   ~ 0
KEYB_1
Text Label 4100 5650 1    60   ~ 0
KEYB_2
Text Label 5100 5650 1    60   ~ 0
KEYB_3
Text Label 6250 4000 2    60   ~ 0
KEYB_4
Text Label 6250 4100 2    60   ~ 0
KEYB_5
Text Label 4500 2350 3    60   ~ 0
AREF
Text Label 4600 5650 1    60   ~ 0
KEYB_7
Text Label 4100 2350 3    60   ~ 0
KEYB_8
Text Label 3850 6150 2    60   ~ 0
KEYB_1
Text Label 3850 6250 2    60   ~ 0
KEYB_2
Text Label 3850 7050 2    60   ~ 0
KEYB_3
Text Label 3850 7350 2    60   ~ 0
KEYB_4
Text Label 3850 7250 2    60   ~ 0
KEYB_5
Text Label 3850 7150 2    60   ~ 0
AREF
Text Label 3850 6450 2    60   ~ 0
KEYB_7
Text Label 3850 6050 2    60   ~ 0
KEYB_8
Wire Wire Line
	3350 6150 3850 6150
Wire Wire Line
	3350 6250 3850 6250
Wire Wire Line
	3350 6350 3850 6350
Wire Wire Line
	3350 6450 3850 6450
Wire Wire Line
	3350 6550 3850 6550
Wire Wire Line
	3350 6650 3850 6650
Wire Wire Line
	3350 6750 3850 6750
Wire Wire Line
	3350 6850 3850 6850
$Comp
L CONN_01X14 P3
U 1 1 55A05372
P 3150 6700
F 0 "P3" H 3150 7450 50  0000 C CNN
F 1 "TMM-6-0-14-2" V 3250 6700 50  0000 C CNN
F 2 "monitor:TMM-6-0-14-2" H 3150 6700 60  0001 C CNN
F 3 "" H 3150 6700 60  0000 C CNN
	1    3150 6700
	-1   0    0    1   
$EndComp
Connection ~ 5700 1800
Connection ~ 2600 900 
Wire Wire Line
	7900 1050 8350 1050
Wire Wire Line
	8350 1650 7900 1650
$Comp
L GND #PWR021
U 1 1 55B74BB0
P 8550 2550
F 0 "#PWR021" H 8550 2550 30  0001 C CNN
F 1 "GND" H 8550 2480 30  0001 C CNN
F 2 "" H 8550 2550 60  0001 C CNN
F 3 "" H 8550 2550 60  0001 C CNN
	1    8550 2550
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X03 P2
U 1 1 55D6F150
P 2000 7250
F 0 "P2" H 2000 7450 50  0000 C CNN
F 1 "CONN_01X03" V 2100 7250 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03" H 2000 7250 60  0001 C CNN
F 3 "" H 2000 7250 60  0000 C CNN
	1    2000 7250
	-1   0    0    1   
$EndComp
NoConn ~ 4900 2900
NoConn ~ 4800 2900
NoConn ~ 4500 5100
Wire Wire Line
	3850 6050 3350 6050
Text Label 5700 900  0    60   ~ 0
VIN
NoConn ~ 4600 2900
$EndSCHEMATC
