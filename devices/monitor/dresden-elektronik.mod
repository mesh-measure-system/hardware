PCBNEW-LibModule-V1  2014-03-20 18:49:13
# encoding utf-8
Units mm
$INDEX
DERFMEGA256-23M00
DERFMEGA256-23M10
$EndINDEX
$MODULE DERFMEGA256-23M00
Po 0 0 0 15 53258332 00000000 ~~
Li DERFMEGA256-23M00
Sc 0
AR 
Op 0 0 0
T0 7 -14.4 1.5 1.5 0 0.15 N V 21 N "DERFMEGA256-23M00"
T1 8.1 1.7 1.5 1.5 0 0.15 N V 21 N "VAL**"
DS 4.6 0 0 0 0.15 21
DS 0 0 0 -13.2 0.15 21
DS 0 -13.2 4.6 -13.2 0.15 21
DS 4.6 0 4.6 -13.2 0.15 21
DS 23.6 0 4.6 0 0.15 21
DS 23.6 0 23.6 -13.2 0.15 21
DS 23.6 -13.2 4.6 -13.2 0.15 21
$PAD
Sh "1" O 1 1.3 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 5.7 -1
$EndPAD
$PAD
Sh "2" O 1 1.3 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 6.5 -2.3
$EndPAD
$PAD
Sh "3" O 1 1.3 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 7.3 -1
$EndPAD
$PAD
Sh "4" O 1 1.3 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 8.1 -2.3
$EndPAD
$PAD
Sh "5" O 1 1.3 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 8.9 -1
$EndPAD
$PAD
Sh "6" O 1 1.3 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 9.7 -2.3
$EndPAD
$PAD
Sh "7" O 1 1.3 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 10.5 -1
$EndPAD
$PAD
Sh "8" O 1 1.3 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 11.3 -2.3
$EndPAD
$PAD
Sh "9" O 1 1.3 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 12.1 -1
$EndPAD
$PAD
Sh "10" O 1 1.3 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 12.9 -2.3
$EndPAD
$PAD
Sh "11" O 1 1.3 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 13.7 -1
$EndPAD
$PAD
Sh "12" O 1 1.3 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 14.5 -2.3
$EndPAD
$PAD
Sh "13" O 1 1.3 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 15.3 -1
$EndPAD
$PAD
Sh "14" O 1 1.3 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 16.1 -2.3
$EndPAD
$PAD
Sh "15" O 1 1.3 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 16.9 -1
$EndPAD
$PAD
Sh "16" O 1 1.3 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 17.7 -2.3
$EndPAD
$PAD
Sh "17" O 1 1.3 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 18.5 -1
$EndPAD
$PAD
Sh "18" O 1 1.3 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 19.3 -2.3
$EndPAD
$PAD
Sh "19" O 1 1.3 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 20.1 -1
$EndPAD
$PAD
Sh "20" O 1 1.3 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 20.9 -2.3
$EndPAD
$PAD
Sh "21" O 1 1.3 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 21.7 -1
$EndPAD
$PAD
Sh "22" O 1 1.3 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 22.5 -2.3
$EndPAD
$PAD
Sh "23" O 1.3 1 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 23.1 -3.8
$EndPAD
$PAD
Sh "24" O 1.3 1 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 21.8 -4.6
$EndPAD
$PAD
Sh "25" O 1.3 1 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 23.1 -5.4
$EndPAD
$PAD
Sh "26" O 1.3 1 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 21.8 -6.2
$EndPAD
$PAD
Sh "27" O 1.3 1 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 23.1 -7
$EndPAD
$PAD
Sh "28" O 1.3 1 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 21.8 -7.8
$EndPAD
$PAD
Sh "29" O 1.3 1 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 23.1 -8.6
$EndPAD
$PAD
Sh "30" O 1.3 1 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 21.8 -9.4
$EndPAD
$PAD
Sh "31" O 1 1.3 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 21.8 -12.2
$EndPAD
$PAD
Sh "32" O 1 1.3 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 21 -10.9
$EndPAD
$PAD
Sh "33" O 1 1.3 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 20.2 -12.2
$EndPAD
$PAD
Sh "34" O 1 1.3 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 19.4 -10.9
$EndPAD
$PAD
Sh "35" O 1 1.3 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 18.6 -12.2
$EndPAD
$PAD
Sh "36" O 1 1.3 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 17.8 -10.9
$EndPAD
$PAD
Sh "37" O 1 1.3 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 17 -12.2
$EndPAD
$PAD
Sh "38" O 1 1.3 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 16.2 -10.9
$EndPAD
$PAD
Sh "39" O 1 1.3 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 15.4 -12.2
$EndPAD
$PAD
Sh "40" O 1 1.3 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 14.6 -10.9
$EndPAD
$PAD
Sh "41" O 1 1.3 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 13.8 -12.2
$EndPAD
$PAD
Sh "42" O 1 1.3 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 13 -10.9
$EndPAD
$PAD
Sh "43" O 1 1.3 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 12.2 -12.2
$EndPAD
$PAD
Sh "44" O 1 1.3 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 11.4 -10.9
$EndPAD
$PAD
Sh "45" O 1 1.3 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 10.6 -12.2
$EndPAD
$PAD
Sh "46" O 1 1.3 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 9.8 -10.9
$EndPAD
$PAD
Sh "47" O 1 1.3 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 9 -12.2
$EndPAD
$PAD
Sh "48" O 1 1.3 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 8.2 -10.9
$EndPAD
$PAD
Sh "49" O 1 1.3 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 7.4 -12.2
$EndPAD
$PAD
Sh "50" O 1 1.3 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 6.6 -10.9
$EndPAD
$PAD
Sh "51" O 1 1.3 0 0 0
Dr 0.6 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 5.8 -12.2
$EndPAD
$EndMODULE DERFMEGA256-23M00
$MODULE DERFMEGA256-23M10
Po 0 0 0 15 532B2A0B 00000000 ~~
Li DERFMEGA256-23M10
Sc 0
AR 
Op 0 0 0
T0 9.8 -14.4 1.5 1.5 0 0.15 N V 21 N "DERFMEGA256-23M10"
T1 10.6 1.5 1.5 1.5 0 0.15 N V 21 N "VAL**"
DS 0 0 0 -13.2 0.15 21
DS 19 0 0 0 0.15 21
DS 19 0 19 -13.2 0.15 21
DS 19 -13.2 0 -13.2 0.15 21
$PAD
Sh "1" R 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1.1 -1
$EndPAD
$PAD
Sh "2" R 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1.9 -2.3
$EndPAD
$PAD
Sh "3" R 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2.7 -1
$EndPAD
$PAD
Sh "4" R 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3.5 -2.3
$EndPAD
$PAD
Sh "5" R 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4.3 -1
$EndPAD
$PAD
Sh "6" R 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 5.1 -2.3
$EndPAD
$PAD
Sh "7" R 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 5.9 -1
$EndPAD
$PAD
Sh "8" R 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 6.7 -2.3
$EndPAD
$PAD
Sh "9" R 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 7.5 -1
$EndPAD
$PAD
Sh "10" R 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 8.3 -2.3
$EndPAD
$PAD
Sh "11" R 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 9.1 -1
$EndPAD
$PAD
Sh "12" R 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 9.9 -2.3
$EndPAD
$PAD
Sh "13" R 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 10.7 -1
$EndPAD
$PAD
Sh "14" R 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 11.5 -2.3
$EndPAD
$PAD
Sh "15" R 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 12.3 -1
$EndPAD
$PAD
Sh "16" R 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 13.1 -2.3
$EndPAD
$PAD
Sh "17" R 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 13.9 -1
$EndPAD
$PAD
Sh "18" R 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 14.7 -2.3
$EndPAD
$PAD
Sh "19" R 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 15.5 -1
$EndPAD
$PAD
Sh "20" R 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 16.3 -2.3
$EndPAD
$PAD
Sh "21" R 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 17.1 -1
$EndPAD
$PAD
Sh "22" R 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 17.9 -2.3
$EndPAD
$PAD
Sh "23" R 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 18.5 -3.8
$EndPAD
$PAD
Sh "24" R 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 17.2 -4.6
$EndPAD
$PAD
Sh "25" R 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 18.5 -5.4
$EndPAD
$PAD
Sh "26" R 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 17.2 -6.2
$EndPAD
$PAD
Sh "27" R 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 18.5 -7
$EndPAD
$PAD
Sh "28" R 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 17.2 -7.8
$EndPAD
$PAD
Sh "29" R 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 18.5 -8.6
$EndPAD
$PAD
Sh "30" R 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 17.2 -9.4
$EndPAD
$PAD
Sh "31" R 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 17.2 -12.2
$EndPAD
$PAD
Sh "32" R 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 16.4 -10.9
$EndPAD
$PAD
Sh "33" R 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 15.6 -12.2
$EndPAD
$PAD
Sh "34" R 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 14.8 -10.9
$EndPAD
$PAD
Sh "35" R 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 14 -12.2
$EndPAD
$PAD
Sh "36" R 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 13.2 -10.9
$EndPAD
$PAD
Sh "37" R 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 12.4 -12.2
$EndPAD
$PAD
Sh "38" R 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 11.6 -10.9
$EndPAD
$PAD
Sh "39" R 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 10.8 -12.2
$EndPAD
$PAD
Sh "40" R 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 10 -10.9
$EndPAD
$PAD
Sh "41" R 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 9.2 -12.2
$EndPAD
$PAD
Sh "42" R 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 8.4 -10.9
$EndPAD
$PAD
Sh "43" R 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 7.6 -12.2
$EndPAD
$PAD
Sh "44" R 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 6.8 -10.9
$EndPAD
$PAD
Sh "45" R 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 6 -12.2
$EndPAD
$PAD
Sh "46" R 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 5.2 -10.9
$EndPAD
$PAD
Sh "47" R 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4.4 -12.2
$EndPAD
$PAD
Sh "48" R 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3.6 -10.9
$EndPAD
$PAD
Sh "49" R 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2.8 -12.2
$EndPAD
$PAD
Sh "50" R 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2 -10.9
$EndPAD
$PAD
Sh "51" R 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1.2 -12.2
$EndPAD
$PAD
Sh "52" O 1.6 0.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3.7 -7.6
$EndPAD
$PAD
Sh "54" O 0.5 1.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4.8 -6.6
$EndPAD
$PAD
Sh "55" O 1.6 0.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3.7 -5.6
$EndPAD
$PAD
Sh "53" O 0.6 0.6 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3.38 -6.6
$EndPAD
$EndMODULE DERFMEGA256-23M10
$EndLIBRARY
