PCBNEW-LibModule-V1  2014-03-25 20:09:32
# encoding utf-8
Units mm
$INDEX
RF-IPX125-1G
$EndINDEX
$MODULE RF-IPX125-1G
Po 0 0 0 15 5331D42C 00000000 ~~
Li RF-IPX125-1G
Sc 0
AR 
Op 0 0 0
T0 0 -2.4 1.5 1.5 0 0.15 N V 21 N "RF-IPX125-1G"
T1 0 3.6 1.5 1.5 0 0.15 N V 21 N "VAL**"
DS -0.3 -0.2 -0.3 0.1 0.15 21
DS -0.3 0.1 0.3 0.1 0.15 21
DS 0.3 0.1 0.3 -0.2 0.15 21
DS 1.3 -2.4 1.5 -2.4 0.15 21
DS 1.5 -2.4 1.5 -0.6 0.15 21
DS 1.5 -0.6 1.3 -0.6 0.15 21
DS -1.3 -2.4 -1.5 -2.4 0.15 21
DS -1.5 -2.4 -1.5 -0.6 0.15 21
DS -1.5 -0.6 -1.3 -0.6 0.15 21
DS -1.3 -2.8 -1.3 -0.2 0.15 21
DS -1.3 -0.2 1.3 -0.2 0.15 21
DS 1.3 -0.2 1.3 -2.8 0.15 21
DS 1.3 -2.8 -1.3 -2.8 0.15 21
$PAD
Sh "1" R 1 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 0
$EndPAD
$PAD
Sh "2" R 1.05 2 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1.5 -1.5
$EndPAD
$PAD
Sh "3" R 1.05 2 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1.5 -1.5
$EndPAD
$EndMODULE RF-IPX125-1G
$EndLIBRARY
