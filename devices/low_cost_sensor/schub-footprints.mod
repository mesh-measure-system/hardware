PCBNEW-LibModule-V1  Wed 08 Aug 2012 11:30:28 CEST
# encoding utf-8
$INDEX
SOT143
15EDGRC-3.5/3P
SMART-CARD
FCI-76382-305
ESD102E
uSDCARD
ESD108E
MCC_SC
SMT-5-40-0.8-M
MCC_SC_SCARD
SMT-5-40-0.8-F
C96ABCFD-SLIM-PADS
$EndINDEX
$MODULE SOT143
Po 0 0 0 15 4FBB6D04 00000000 ~~
Li SOT143
Sc 00000000
AR SOT143
Op 0 0 0
T0 315 787 394 394 0 79 N V 21 N "SOT143"
T1 472 -1575 394 394 0 79 N V 21 N "VAL**"
DS -276 -236 -276 -788 79 21
DS -276 -788 906 -788 79 21
DS 906 -788 906 -236 79 21
DS 906 -236 -276 -236 79 21
$PAD
Sh "1" R 512 512 0 0 0
Dr 0 0 0
At STD N 00E08000
Ne 0 ""
Po 0 0
$EndPAD
$PAD
Sh "2" R 394 315 0 0 0
Dr 0 0 0
At STD N 00E08000
Ne 0 ""
Po 669 -40
$EndPAD
$PAD
Sh "3" R 394 315 0 0 0
Dr 0 0 0
At STD N 00E08000
Ne 0 ""
Po 669 -1024
$EndPAD
$PAD
Sh "4" R 394 315 0 0 0
Dr 0 0 0
At STD N 00E08000
Ne 0 ""
Po -79 -1024
$EndPAD
$EndMODULE  SOT143
$MODULE 15EDGRC-3.5/3P
Po 0 0 0 15 4FBB919B 00000000 ~~
Li 15EDGRC-3.5/3P
Sc 00000000
AR
Op 0 0 0
T0 1378 1083 600 600 0 120 N V 21 N "15EDGRC-3.5/3P"
T1 1575 -1772 600 600 0 120 N V 21 N "VAL**"
DS -689 -492 3445 -492 79 21
DS -689 -3150 3445 -3150 79 21
DS -689 492 -689 -3150 79 21
DS -689 492 3445 492 79 21
DS 3445 492 3445 -3150 79 21
$PAD
Sh "1" R 984 984 0 0 0
Dr 551 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 0
$EndPAD
$PAD
Sh "2" C 984 984 0 0 0
Dr 551 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 1378 0
$EndPAD
$PAD
Sh "3" C 984 984 0 0 0
Dr 551 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 2756 0
$EndPAD
$EndMODULE  15EDGRC-3.5/3P
$MODULE SMART-CARD
Po 0 0 0 15 4FBC9BF7 00000000 ~~
Li SMART-CARD
Sc 00000000
AR SMART-CARD
Op 0 0 0
T0 16929 20472 600 600 0 120 N V 21 N "SMART-CARD"
T1 16929 787 600 600 0 120 N V 21 N "VAL**"
DS 5945 7875 5945 11064 1063 15
DS 7402 7914 5906 7914 787 15
DS 33465 21260 33465 0 79 15
DS 0 21260 33465 21260 79 15
DS 0 0 33465 0 79 15
DS 0 0 0 21260 79 15
$PAD
Sh "C1" R 1575 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4429 7906
$EndPAD
$PAD
Sh "C2" R 1575 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4429 8906
$EndPAD
$PAD
Sh "C3" R 1575 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4429 9906
$EndPAD
$PAD
Sh "C4" R 1575 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4429 10906
$EndPAD
$PAD
Sh "C5" R 1575 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 7429 7906
$EndPAD
$PAD
Sh "C6" R 1575 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 7429 8906
$EndPAD
$PAD
Sh "C7" R 1575 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 7429 9906
$EndPAD
$PAD
Sh "C8" R 1575 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 7429 10906
$EndPAD
$EndMODULE  SMART-CARD
$MODULE FCI-76382-305
Po 0 0 0 15 4FBDF96B 00000000 ~~
Li FCI-76382-305
Sc 00000000
AR
Op 0 0 0
T0 -2000 1000 600 600 0 120 N V 21 N "FCI-76382-305"
T1 -1750 -5000 600 600 0 120 N V 21 N "VAL**"
DS -4252 -4488 236 -4488 157 21
DS -4488 -551 -4488 -3622 157 21
DS -4488 -3622 -4252 -4488 157 21
DS 472 -551 472 -3543 157 21
DS 472 -3543 236 -4488 157 21
DS 472 -551 -4488 -551 157 21
$PAD
Sh "1" R 787 787 0 0 0
Dr 394 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 0
$EndPAD
$PAD
Sh "2" C 787 787 0 0 0
Dr 394 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -1000 0
$EndPAD
$PAD
Sh "3" C 787 787 0 0 0
Dr 394 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -2000 0
$EndPAD
$PAD
Sh "4" C 787 787 0 0 0
Dr 394 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -3000 0
$EndPAD
$PAD
Sh "5" C 787 787 0 0 0
Dr 394 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -4000 0
$EndPAD
$EndMODULE  FCI-76382-305
$MODULE ESD102E
Po 0 0 0 15 4FBE099A 00000000 ~~
Li ESD102E
Sc 00000000
AR
Op 0 0 0
T0 2250 -2000 600 600 900 120 N V 21 N "ESD102E"
T1 -1500 -2500 600 600 900 120 N V 21 N "VAL**"
T2 500 -2700 600 600 0 120 N V 21 N "ON"
DS 1200 -1900 800 -1900 157 21
DS 800 -2100 1200 -2100 157 21
DS 200 -2100 -200 -2100 157 21
DS -200 -1900 200 -1900 157 21
DS -250 -2000 250 -2000 157 21
DS 750 -2000 1250 -2000 157 21
DS 750 -1750 1250 -1750 157 21
DS 750 -2250 750 -1000 157 21
DS 750 -1000 1250 -1000 157 21
DS 1250 -1000 1250 -2250 157 21
DS 1250 -2250 750 -2250 157 21
DS 250 -1750 -250 -1750 157 21
DS 250 -1000 -250 -1000 157 21
DS -250 -1000 -250 -2250 157 21
DS -250 -2250 250 -2250 157 21
DS 250 -2250 250 -1000 157 21
DS -500 -3250 -500 -500 157 21
DS 1500 -3250 1500 -500 157 21
DS -500 -500 1500 -500 157 21
DS 1500 -3250 -500 -3250 157 21
$PAD
Sh "1" R 630 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 0
$EndPAD
$PAD
Sh "2" R 630 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1000 0
$EndPAD
$PAD
Sh "3" R 630 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1000 -3750
$EndPAD
$PAD
Sh "4" R 630 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 -3750
$EndPAD
$EndMODULE  ESD102E
$MODULE uSDCARD
Po 0 0 0 15 4FF81882 00000000 ~~
Li uSDCARD
Sc 00000000
AR
Op 0 0 0
T0 0 6299 600 600 0 120 N V 21 N "uSDCARD"
T1 0 -1339 600 600 0 120 N V 21 N "VAL**"
DS -2835 4803 -2835 7480 150 21
DS -2835 7480 2756 7480 150 21
DS 2756 7480 2756 4803 150 21
DS -2835 0 -2835 4803 150 21
DS -2835 4803 2756 4803 150 21
DS 2756 4803 2756 0 150 21
DS -2835 0 2756 0 150 21
$PAD
Sh "1" R 276 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1161 -394
$EndPAD
$PAD
Sh "2" R 276 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 728 -394
$EndPAD
$PAD
Sh "3" R 276 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 295 -394
$EndPAD
$PAD
Sh "4" R 276 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -138 -394
$EndPAD
$PAD
Sh "5" R 276 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -571 -394
$EndPAD
$PAD
Sh "6" R 276 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1004 -394
$EndPAD
$PAD
Sh "7" R 276 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1437 -394
$EndPAD
$PAD
Sh "8" R 276 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1870 -394
$EndPAD
$PAD
Sh "9" R 276 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1594 -394
$EndPAD
$PAD
Sh "10" R 276 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2027 -394
$EndPAD
$PAD
Sh "100" C 748 748 0 0 0
Dr 591 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -1969 1024
$EndPAD
$PAD
Sh "101" C 748 748 0 0 0
Dr 591 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 1969 1024
$EndPAD
$PAD
Sh "102" R 472 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2638 2913
$EndPAD
$PAD
Sh "103" R 472 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2638 2913
$EndPAD
$EndMODULE  uSDCARD
$MODULE ESD108E
Po 0 0 0 15 5017CB5B 00000000 ~~
Li ESD108E
Sc 00000000
AR ESD102E
Op 0 0 0
T0 8000 -1750 600 600 900 120 N V 21 N "ESD108E"
T1 -1000 -2250 600 600 900 120 N V 21 N "VAL**"
DS 4250 -2250 4250 -1000 157 21
DS 3750 -2250 4250 -2250 157 21
DS 3750 -1000 3750 -2250 157 21
DS 4250 -1000 3750 -1000 157 21
DS 4250 -1750 3750 -1750 157 21
DS 5250 -2250 4750 -2250 157 21
DS 5250 -1000 5250 -2250 157 21
DS 4750 -1000 5250 -1000 157 21
DS 4750 -2250 4750 -1000 157 21
DS 4750 -1750 5250 -1750 157 21
DS 4750 -2000 5250 -2000 157 21
DS 3750 -2000 4250 -2000 157 21
DS 3800 -1900 4200 -1900 157 21
DS 4200 -2100 3800 -2100 157 21
DS 4800 -2100 5200 -2100 157 21
DS 5200 -1900 4800 -1900 157 21
DS 7200 -1900 6800 -1900 157 21
DS 6800 -2100 7200 -2100 157 21
DS 6200 -2100 5800 -2100 157 21
DS 5800 -1900 6200 -1900 157 21
DS 5750 -2000 6250 -2000 157 21
DS 6750 -2000 7250 -2000 157 21
DS 6750 -1750 7250 -1750 157 21
DS 6750 -2250 6750 -1000 157 21
DS 6750 -1000 7250 -1000 157 21
DS 7250 -1000 7250 -2250 157 21
DS 7250 -2250 6750 -2250 157 21
DS 6250 -1750 5750 -1750 157 21
DS 6250 -1000 5750 -1000 157 21
DS 5750 -1000 5750 -2250 157 21
DS 5750 -2250 6250 -2250 157 21
DS 6250 -2250 6250 -1000 157 21
DS 2250 -2250 2250 -1000 157 21
DS 1750 -2250 2250 -2250 157 21
DS 1750 -1000 1750 -2250 157 21
DS 2250 -1000 1750 -1000 157 21
DS 2250 -1750 1750 -1750 157 21
DS 3250 -2250 2750 -2250 157 21
DS 3250 -1000 3250 -2250 157 21
DS 2750 -1000 3250 -1000 157 21
DS 2750 -2250 2750 -1000 157 21
DS 2750 -1750 3250 -1750 157 21
DS 2750 -2000 3250 -2000 157 21
DS 1750 -2000 2250 -2000 157 21
DS 1800 -1900 2200 -1900 157 21
DS 2200 -2100 1800 -2100 157 21
DS 2800 -2100 3200 -2100 157 21
DS 3200 -1900 2800 -1900 157 21
DS -500 -3250 7500 -3250 150 21
DS -500 -500 7500 -500 150 21
T2 3500 -2750 600 600 0 120 N V 21 N "ON"
DS 1200 -1900 800 -1900 157 21
DS 800 -2100 1200 -2100 157 21
DS 200 -2100 -200 -2100 157 21
DS -200 -1900 200 -1900 157 21
DS -250 -2000 250 -2000 157 21
DS 750 -2000 1250 -2000 157 21
DS 750 -1750 1250 -1750 157 21
DS 750 -2250 750 -1000 157 21
DS 750 -1000 1250 -1000 157 21
DS 1250 -1000 1250 -2250 157 21
DS 1250 -2250 750 -2250 157 21
DS 250 -1750 -250 -1750 157 21
DS 250 -1000 -250 -1000 157 21
DS -250 -1000 -250 -2250 157 21
DS -250 -2250 250 -2250 157 21
DS 250 -2250 250 -1000 157 21
DS -500 -3250 -500 -500 157 21
DS 7500 -3250 7500 -500 157 21
$PAD
Sh "1" R 630 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 0
$EndPAD
$PAD
Sh "2" R 630 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1000 0
$EndPAD
$PAD
Sh "15" R 630 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1000 -3750
$EndPAD
$PAD
Sh "16" R 630 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 -3750
$EndPAD
$PAD
Sh "3" R 630 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2000 0
$EndPAD
$PAD
Sh "4" R 630 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3000 0
$EndPAD
$PAD
Sh "5" R 630 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4000 0
$EndPAD
$PAD
Sh "6" R 630 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 5000 0
$EndPAD
$PAD
Sh "7" R 630 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 6000 0
$EndPAD
$PAD
Sh "8" R 630 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 7000 0
$EndPAD
$PAD
Sh "9" R 630 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 7000 -3750
$EndPAD
$PAD
Sh "10" R 630 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 6000 -3750
$EndPAD
$PAD
Sh "11" R 630 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 5000 -3750
$EndPAD
$PAD
Sh "12" R 630 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4000 -3750
$EndPAD
$PAD
Sh "13" R 630 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3000 -3750
$EndPAD
$PAD
Sh "14" R 630 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2000 -3750
$EndPAD
$EndMODULE  ESD108E
$MODULE MCC_SC
Po 0 0 0 15 5017D711 00000000 ~~
Li MCC_SC
Sc 00000000
AR
Op 0 0 0
T0 -2500 -750 600 600 0 120 N V 21 N "MCC_SC"
T1 -3250 -1750 600 600 0 120 N V 21 N "VAL**"
DS 0 0 0 -8500 150 21
DS 0 -8500 -1000 -12750 150 21
DS -1000 -12750 -5750 -12750 150 21
DS -5750 -12750 -5750 -13500 150 21
DS -5750 -13500 -10750 -13500 150 21
DS -10750 -13500 -10750 -12750 150 21
DS -10750 -12750 -22000 -12750 150 21
DS 0 0 -23000 0 150 21
DS -23000 0 -23000 -8500 150 21
DS -23000 -8500 -22000 -12750 150 21
$PAD
Sh "C1" R 787 1969 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -14250 -10600
$EndPAD
$PAD
Sh "C2" R 787 1969 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -13250 -10600
$EndPAD
$PAD
Sh "C3" R 787 1969 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -12250 -10600
$EndPAD
$PAD
Sh "C4" R 787 1969 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -11250 -10600
$EndPAD
$PAD
Sh "C5" R 787 1969 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -14250 -600
$EndPAD
$PAD
Sh "C6" R 787 1969 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -13250 -600
$EndPAD
$PAD
Sh "C7" R 787 1969 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -12250 -600
$EndPAD
$PAD
Sh "C8" R 787 1969 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -11250 -600
$EndPAD
$PAD
Sh "S1" R 787 1969 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -9250 -8000
$EndPAD
$PAD
Sh "S2" R 787 1969 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -8250 -8000
$EndPAD
$PAD
Sh "A1" C 1181 1181 0 0 0
Dr 866 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -2250 -9750
$EndPAD
$PAD
Sh "A2" C 1181 1181 0 0 0
Dr 866 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -19750 -1750
$EndPAD
$EndMODULE  MCC_SC
$MODULE SMT-5-40-0.8-M
Po 0 0 0 15 5017DDB2 00000000 ~~
Li SMT-5-40-0.8-M
Sc 00000000
AR
Op 0 0 0
T0 0 -3465 600 600 0 120 N V 21 N "SMT-5-40-0.8-M"
T1 0 -1732 600 600 0 120 N V 21 N "VAL**"
DS 591 -197 7283 -197 98 21
DS 7283 -197 7283 197 98 21
DS 7283 197 591 197 98 21
DS 591 197 591 -197 98 21
DS -394 -591 0 -591 98 21
DS 0 -591 0 -1378 98 21
DS 0 -1378 7874 -1378 98 21
DS 7874 -1378 7874 -591 98 21
DS 7874 -591 8465 -591 98 21
DS 8465 -591 8465 1378 98 21
DS 8465 1378 -394 1378 98 21
DS -394 1378 -394 -591 98 21
$PAD
Sh "A2" C 787 787 0 0 0
Dr 512 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 7953 0
$EndPAD
$PAD
Sh "1" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 984 -866
$EndPAD
$PAD
Sh "2" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 984 866
$EndPAD
$PAD
Sh "3" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1299 -866
$EndPAD
$PAD
Sh "4" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1299 866
$EndPAD
$PAD
Sh "5" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1614 -866
$EndPAD
$PAD
Sh "6" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1614 866
$EndPAD
$PAD
Sh "7" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1929 -866
$EndPAD
$PAD
Sh "8" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1929 866
$EndPAD
$PAD
Sh "9" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2244 -866
$EndPAD
$PAD
Sh "10" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2244 866
$EndPAD
$PAD
Sh "11" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2559 -866
$EndPAD
$PAD
Sh "12" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2559 866
$EndPAD
$PAD
Sh "13" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2874 -866
$EndPAD
$PAD
Sh "14" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2874 866
$EndPAD
$PAD
Sh "15" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3189 -866
$EndPAD
$PAD
Sh "16" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3189 866
$EndPAD
$PAD
Sh "17" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3504 -866
$EndPAD
$PAD
Sh "18" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3504 866
$EndPAD
$PAD
Sh "19" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3819 -866
$EndPAD
$PAD
Sh "20" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3819 866
$EndPAD
$PAD
Sh "21" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4134 -866
$EndPAD
$PAD
Sh "22" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4134 866
$EndPAD
$PAD
Sh "23" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4449 -866
$EndPAD
$PAD
Sh "24" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4449 866
$EndPAD
$PAD
Sh "25" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4764 -866
$EndPAD
$PAD
Sh "26" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4764 866
$EndPAD
$PAD
Sh "27" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 5078 -866
$EndPAD
$PAD
Sh "28" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 5078 866
$EndPAD
$PAD
Sh "29" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 5393 -866
$EndPAD
$PAD
Sh "30" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 5393 866
$EndPAD
$PAD
Sh "31" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 5708 -866
$EndPAD
$PAD
Sh "32" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 5708 866
$EndPAD
$PAD
Sh "33" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 6023 -866
$EndPAD
$PAD
Sh "34" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 6023 866
$EndPAD
$PAD
Sh "35" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 6338 -866
$EndPAD
$PAD
Sh "36" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 6338 866
$EndPAD
$PAD
Sh "37" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 6653 -866
$EndPAD
$PAD
Sh "38" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 6653 866
$EndPAD
$PAD
Sh "39" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 6968 -866
$EndPAD
$PAD
Sh "40" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 6968 866
$EndPAD
$PAD
Sh "A1" C 787 787 0 0 0
Dr 512 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 0
$EndPAD
$EndMODULE  SMT-5-40-0.8-M
$MODULE MCC_SC_SCARD
Po 0 0 0 15 5018CFC7 00000000 ~~
Li MCC_SC_SCARD
Sc 00000000
AR
Op 0 0 0
T0 -2500 -750 600 600 0 120 N V 21 N "MCC_SC"
T1 -3250 -1750 600 600 0 120 N V 21 N "VAL**"
DS -787 -11417 -22047 -11417 39 21
DS -22047 -11417 -22047 22441 39 21
DS -22047 22441 -787 22441 39 21
DS -787 22441 -787 -11417 39 21
DS 0 0 0 -8500 150 21
DS 0 -8500 -1000 -12750 150 21
DS -1000 -12750 -5750 -12750 150 21
DS -5750 -12750 -5750 -13500 150 21
DS -5750 -13500 -10750 -13500 150 21
DS -10750 -13500 -10750 -12750 150 21
DS -10750 -12750 -22000 -12750 150 21
DS 0 0 -23000 0 150 21
DS -23000 0 -23000 -8500 150 21
DS -23000 -8500 -22000 -12750 150 21
$PAD
Sh "C1" R 787 1969 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -14250 -10600
$EndPAD
$PAD
Sh "C2" R 787 1969 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -13250 -10600
$EndPAD
$PAD
Sh "C3" R 787 1969 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -12250 -10600
$EndPAD
$PAD
Sh "C4" R 787 1969 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -11250 -10600
$EndPAD
$PAD
Sh "C5" R 787 1969 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -14250 -600
$EndPAD
$PAD
Sh "C6" R 787 1969 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -13250 -600
$EndPAD
$PAD
Sh "C7" R 787 1969 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -12250 -600
$EndPAD
$PAD
Sh "C8" R 787 1969 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -11250 -600
$EndPAD
$PAD
Sh "S1" R 787 1969 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -9250 -8000
$EndPAD
$PAD
Sh "S2" R 787 1969 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -8250 -8000
$EndPAD
$PAD
Sh "A1" C 1181 1181 0 0 0
Dr 866 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -2250 -9750
$EndPAD
$PAD
Sh "A2" C 1181 1181 0 0 0
Dr 866 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -19750 -1750
$EndPAD
$EndMODULE  MCC_SC_SCARD
$MODULE SMT-5-40-0.8-F
Po 0 0 0 15 5020EAAE 00000000 ~~
Li SMT-5-40-0.8-F
Sc 00000000
AR SMT-5-40-0.8-M
Op 0 0 0
T0 3937 -1772 600 600 0 120 N V 21 N "SMT-5-40-0.8-F"
T1 4331 1969 600 600 0 120 N V 21 N "VAL**"
DS 591 -197 7283 -197 98 21
DS 7283 -197 7283 197 98 21
DS 7283 197 591 197 98 21
DS 591 197 591 -197 98 21
DS -394 591 0 591 98 21
DS 0 1377 0 590 98 21
DS 1 1378 7875 1378 98 21
DS 7874 590 7874 1377 98 21
DS 7874 591 8465 591 98 21
DS 8465 -1379 8465 590 98 21
DS 8465 -1378 -394 -1378 98 21
DS -394 591 -394 -1378 98 21
$PAD
Sh "A2" C 787 787 0 0 0
Dr 512 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 7953 0
$EndPAD
$PAD
Sh "1" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 984 866
$EndPAD
$PAD
Sh "2" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 984 -866
$EndPAD
$PAD
Sh "3" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1299 866
$EndPAD
$PAD
Sh "4" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1299 -866
$EndPAD
$PAD
Sh "5" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1614 866
$EndPAD
$PAD
Sh "6" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1614 -866
$EndPAD
$PAD
Sh "7" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1929 866
$EndPAD
$PAD
Sh "8" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1929 -866
$EndPAD
$PAD
Sh "9" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2244 866
$EndPAD
$PAD
Sh "10" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2244 -866
$EndPAD
$PAD
Sh "11" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2559 866
$EndPAD
$PAD
Sh "12" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2559 -866
$EndPAD
$PAD
Sh "13" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2874 866
$EndPAD
$PAD
Sh "14" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2874 -866
$EndPAD
$PAD
Sh "15" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3189 866
$EndPAD
$PAD
Sh "16" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3189 -866
$EndPAD
$PAD
Sh "17" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3504 866
$EndPAD
$PAD
Sh "18" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3504 -866
$EndPAD
$PAD
Sh "19" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3819 866
$EndPAD
$PAD
Sh "20" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3819 -866
$EndPAD
$PAD
Sh "21" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4134 866
$EndPAD
$PAD
Sh "22" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4134 -866
$EndPAD
$PAD
Sh "23" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4449 866
$EndPAD
$PAD
Sh "24" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4449 -866
$EndPAD
$PAD
Sh "25" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4764 866
$EndPAD
$PAD
Sh "26" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4764 -866
$EndPAD
$PAD
Sh "27" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 5078 866
$EndPAD
$PAD
Sh "28" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 5078 -866
$EndPAD
$PAD
Sh "29" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 5393 866
$EndPAD
$PAD
Sh "30" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 5393 -866
$EndPAD
$PAD
Sh "31" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 5708 866
$EndPAD
$PAD
Sh "32" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 5708 -866
$EndPAD
$PAD
Sh "33" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 6023 866
$EndPAD
$PAD
Sh "34" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 6023 -866
$EndPAD
$PAD
Sh "35" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 6338 866
$EndPAD
$PAD
Sh "36" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 6338 -866
$EndPAD
$PAD
Sh "37" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 6653 866
$EndPAD
$PAD
Sh "38" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 6653 -866
$EndPAD
$PAD
Sh "39" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 6968 866
$EndPAD
$PAD
Sh "40" R 197 787 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 6968 -866
$EndPAD
$PAD
Sh "A1" C 787 787 0 0 0
Dr 512 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 0
$EndPAD
$EndMODULE  SMT-5-40-0.8-F
$MODULE C96ABCFD-SLIM-PADS
Po 0 0 0 15 502231AE 00000000 ~~
Li C96ABCFD-SLIM-PADS
Cd Connecteur DIN Europe 96 contacts ABC male droit
Kw CONN DIN
Sc 00000000
AR CON**
Op 0 0 0
T0 -10250 -3000 700 700 0 120 N V 21 N "CON**"
T1 8250 -3000 700 700 0 120 N V 21 N "C96ABCFD"
DS -18500 2000 18500 2000 120 21
DS 18500 2000 18500 -1000 120 21
DS -18500 -1000 -18500 2000 120 21
DS -16500 -1000 -16500 2000 120 21
DS 16500 -1000 16500 2000 120 21
DS -18500 -1000 -18500 -2000 120 21
DS -18500 -2000 18500 -2000 120 21
DS 18500 -2000 18500 -1000 120 21
DS 16500 -2000 16500 -1000 120 21
DS -16500 -1000 -16500 -2000 120 21
$PAD
Sh "65" C 1500 1500 0 0 0
Dr 1063 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -17500 0
$EndPAD
$PAD
Sh "66" C 1500 1500 0 0 0
Dr 1063 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 17500 0
$EndPAD
$PAD
Sh "A3" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -13500 1000
$EndPAD
$PAD
Sh "A4" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -12500 1000
$EndPAD
$PAD
Sh "A5" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -11500 1000
$EndPAD
$PAD
Sh "A6" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -10500 1000
$EndPAD
$PAD
Sh "A7" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -9500 1000
$EndPAD
$PAD
Sh "A8" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -8500 1000
$EndPAD
$PAD
Sh "A9" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -7500 1000
$EndPAD
$PAD
Sh "A10" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -6500 1000
$EndPAD
$PAD
Sh "A11" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -5500 1000
$EndPAD
$PAD
Sh "A12" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -4500 1000
$EndPAD
$PAD
Sh "A13" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -3500 1000
$EndPAD
$PAD
Sh "A14" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -2500 1000
$EndPAD
$PAD
Sh "A15" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -1500 1000
$EndPAD
$PAD
Sh "A16" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -500 1000
$EndPAD
$PAD
Sh "A17" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 500 1000
$EndPAD
$PAD
Sh "A18" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 1500 1000
$EndPAD
$PAD
Sh "A19" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 2500 1000
$EndPAD
$PAD
Sh "A20" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 3500 1000
$EndPAD
$PAD
Sh "A21" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 4500 1000
$EndPAD
$PAD
Sh "A22" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 5500 1000
$EndPAD
$PAD
Sh "A23" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 6500 1000
$EndPAD
$PAD
Sh "A24" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 7500 1000
$EndPAD
$PAD
Sh "A25" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 8500 1000
$EndPAD
$PAD
Sh "A26" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 9500 1000
$EndPAD
$PAD
Sh "A27" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 10500 1000
$EndPAD
$PAD
Sh "A28" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 11500 1000
$EndPAD
$PAD
Sh "A29" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 12500 1000
$EndPAD
$PAD
Sh "A30" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 13500 1000
$EndPAD
$PAD
Sh "A31" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 14500 1000
$EndPAD
$PAD
Sh "A32" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 15500 1000
$EndPAD
$PAD
Sh "A2" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -14500 1000
$EndPAD
$PAD
Sh "A1" R 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -15500 1000
$EndPAD
$PAD
Sh "B1" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -15500 0
$EndPAD
$PAD
Sh "B2" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -14500 0
$EndPAD
$PAD
Sh "B3" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -13500 0
$EndPAD
$PAD
Sh "B4" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -12500 0
$EndPAD
$PAD
Sh "B5" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -11500 0
$EndPAD
$PAD
Sh "B6" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -10500 0
$EndPAD
$PAD
Sh "B7" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -9500 0
$EndPAD
$PAD
Sh "B8" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -8500 0
$EndPAD
$PAD
Sh "B9" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -7500 0
$EndPAD
$PAD
Sh "B10" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -6500 0
$EndPAD
$PAD
Sh "B11" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -5500 0
$EndPAD
$PAD
Sh "B12" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -4500 0
$EndPAD
$PAD
Sh "B13" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -3500 0
$EndPAD
$PAD
Sh "B14" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -2500 0
$EndPAD
$PAD
Sh "B15" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -1500 0
$EndPAD
$PAD
Sh "B16" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -500 0
$EndPAD
$PAD
Sh "B17" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 500 0
$EndPAD
$PAD
Sh "B18" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 1500 0
$EndPAD
$PAD
Sh "B19" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 2500 0
$EndPAD
$PAD
Sh "B20" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 3500 0
$EndPAD
$PAD
Sh "B21" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 4500 0
$EndPAD
$PAD
Sh "B22" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 5500 0
$EndPAD
$PAD
Sh "B23" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 6500 0
$EndPAD
$PAD
Sh "B24" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 7500 0
$EndPAD
$PAD
Sh "B25" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 8500 0
$EndPAD
$PAD
Sh "B26" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 9500 0
$EndPAD
$PAD
Sh "B27" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 10500 0
$EndPAD
$PAD
Sh "B28" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 11500 0
$EndPAD
$PAD
Sh "B29" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 12500 0
$EndPAD
$PAD
Sh "B30" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 13500 0
$EndPAD
$PAD
Sh "B31" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 14500 0
$EndPAD
$PAD
Sh "B32" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 15500 0
$EndPAD
$PAD
Sh "C1" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -15500 -1000
$EndPAD
$PAD
Sh "C2" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -14500 -1000
$EndPAD
$PAD
Sh "C3" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -13500 -1000
$EndPAD
$PAD
Sh "C4" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -12500 -1000
$EndPAD
$PAD
Sh "C5" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -11500 -1000
$EndPAD
$PAD
Sh "C6" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -10500 -1000
$EndPAD
$PAD
Sh "C7" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -9500 -1000
$EndPAD
$PAD
Sh "C8" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -8500 -1000
$EndPAD
$PAD
Sh "C9" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -7500 -1000
$EndPAD
$PAD
Sh "C10" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -6500 -1000
$EndPAD
$PAD
Sh "C11" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -5500 -1000
$EndPAD
$PAD
Sh "C12" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -4500 -1000
$EndPAD
$PAD
Sh "C13" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -3500 -1000
$EndPAD
$PAD
Sh "C14" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -2500 -1000
$EndPAD
$PAD
Sh "C15" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -1500 -1000
$EndPAD
$PAD
Sh "C16" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -500 -1000
$EndPAD
$PAD
Sh "C17" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 500 -1000
$EndPAD
$PAD
Sh "C18" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 1500 -1000
$EndPAD
$PAD
Sh "C19" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 2500 -1000
$EndPAD
$PAD
Sh "C20" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 3500 -1000
$EndPAD
$PAD
Sh "C21" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 4500 -1000
$EndPAD
$PAD
Sh "C22" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 5500 -1000
$EndPAD
$PAD
Sh "C23" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 6500 -1000
$EndPAD
$PAD
Sh "C24" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 7500 -1000
$EndPAD
$PAD
Sh "C25" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 8500 -1000
$EndPAD
$PAD
Sh "C26" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 9500 -1000
$EndPAD
$PAD
Sh "C27" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 10500 -1000
$EndPAD
$PAD
Sh "C28" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 11500 -1000
$EndPAD
$PAD
Sh "C29" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 12500 -1000
$EndPAD
$PAD
Sh "C30" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 13500 -1000
$EndPAD
$PAD
Sh "C31" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 14500 -1000
$EndPAD
$PAD
Sh "C32" O 500 850 0 0 0
Dr 295 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 15500 -1000
$EndPAD
$SHAPE3D
Na "conn_europe/c96abcfd.wrl"
Sc 1.000000 1.000000 1.000000
Of 0.000000 0.000000 0.000000
Ro 0.000000 0.000000 0.000000
$EndSHAPE3D
$EndMODULE  C96ABCFD-SLIM-PADS
$EndLIBRARY
